/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */
#include "lali.h"

static int instant = 0;
static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
static int centered = 0;                    /* -c option; centers dmenu on screen */
static int min_width = 820;                 /* minimum width when centered */
/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
    "Iosevka Term:Bold:size=9.5:antialias:true:autohint:true:lcdfilter=1",
    // "PragmataProMonoLiga Nerd Font:Bold:size=9:antialias=true:autohint=true:lcdfilter=1",
    // "CascadiaMono:SemiBold:pixelsize=11:antialias=true:autohint=true:lcdfilter=1",
    "JoyPixels:pixelsize=10:antialias=true:autohint=true:lcdfilter=1"
    // "TwitterColorEmoji:pixelsize=10:antialias=true:autohint=true"
};
static const char *prompt      = 0;      /* -p  option; prompt to the left of input field */
static const char *colors[SchemeLast][2] = {
    /*                  fg          bg       */
    [SchemeNorm] = { lightyellow,   black },
    [SchemeSel] = {  gold,          darkerpink },
    [SchemeOut] = { "#000000",      "#00ffff" },
};
/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines      = 0;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/* Size of the window border */
static unsigned int border_width = 3;
